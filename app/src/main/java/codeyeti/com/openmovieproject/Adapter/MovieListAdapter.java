package codeyeti.com.openmovieproject.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import codeyeti.com.openmovieproject.Model.HistoryModel;
import codeyeti.com.openmovieproject.R;

/**
 * Created by ravi on 19/5/16.
 */
public class MovieListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<HistoryModel> historyModels = new ArrayList<>();

    public MovieListAdapter(ArrayList<HistoryModel> historyModels) {
        this.historyModels = historyModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_row_history, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof MyViewHolder)
        {
            final MyViewHolder myViewHolder = (MyViewHolder) holder;
//            myViewHolder.snoTv.setText(String.valueOf(historyModels.get(position).getSno()));
            myViewHolder.dateTV.setText(historyModels.get(position).getDate() +"\n" + historyModels.get(position).getTime() );
            myViewHolder.titleTV.setText(historyModels.get(position).getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return historyModels.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTV, dateTV;
        public MyViewHolder(View view) {
            super(view);
            titleTV = (TextView) view.findViewById(R.id.titleTv);
            dateTV = (TextView) view.findViewById(R.id.dateTv);
//            snoTv = (TextView) view.findViewById(R.id.snoTv);
        }
    }


}
