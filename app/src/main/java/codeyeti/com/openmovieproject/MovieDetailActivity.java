package codeyeti.com.openmovieproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONObject;

import codeyeti.com.openmovieproject.Controller.AppController;
import codeyeti.com.openmovieproject.Controller.Constants;
import codeyeti.com.openmovieproject.Model.MovieListModel;

public class MovieDetailActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout mainContentLL;
    SharedPreferences sharedPreferences;
    String url;
    Toolbar toolbar;
    ImageView logIV,barIV;
    TextView titleTV;

    ImageView posterIV;
    EditText titleET, yearET ,ratedET,releasedET,runtimeET,genreET,directorET, writerET, actorsET, plotET;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        init();

    }

    private void init() {
        logIV = (ImageView) findViewById(R.id.logIV);
        barIV = (ImageView) findViewById(R.id.barIV);
        titleTV = (TextView) findViewById(R.id.toolbar_title);
        titleTV.setText("Movie Details");
        logIV.setOnClickListener(this);
        barIV.setOnClickListener(this);
        toolbar  = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mainContentLL  = (LinearLayout) findViewById(R.id.mainContentLL);
        sharedPreferences = getApplicationContext().getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        titleET = (EditText)findViewById(R.id.titleET);
        yearET = (EditText) findViewById(R.id.yearET);
        ratedET = (EditText) findViewById(R.id.ratedET);
        releasedET = (EditText) findViewById(R.id.releasedET);
        runtimeET = (EditText) findViewById(R.id.runtimeET);
        genreET = (EditText) findViewById(R.id.genreET);
        directorET = (EditText) findViewById(R.id.directorET);
        writerET = (EditText) findViewById(R.id.writerET);
        actorsET = (EditText) findViewById(R.id.actorsET);
        plotET = (EditText) findViewById(R.id.plotET);
        posterIV = (ImageView) findViewById(R.id.posterIV);
    }

    @Override
    protected void onResume() {
        init();
        super.onResume();
            getMovieList();

    }

    private void getMovieList() {


                        titleET.setText(getIntent().getStringExtra(Constants.title));
                        yearET.setText(getIntent().getStringExtra(Constants.year));
                        ratedET.setText(getIntent().getStringExtra(Constants.rated));
                        releasedET.setText(getIntent().getStringExtra(Constants.released));
                        runtimeET.setText(getIntent().getStringExtra(Constants.runtime));
                        genreET.setText(getIntent().getStringExtra(Constants.genre));
                        directorET.setText(getIntent().getStringExtra(Constants.director));
                        writerET.setText(getIntent().getStringExtra(Constants.writer));
                        actorsET.setText(getIntent().getStringExtra(Constants.actors));
                        plotET.setText(getIntent().getStringExtra(Constants.plot));
                        String url = getIntent().getStringExtra(Constants.imageUrl);
                        if(url==null)
                        {
                            Glide.with(getApplicationContext()).load(R.drawable.default_movie).into(posterIV);
                        }
                        else if(url.equals("N/A"))
                        {
                            Glide.with(getApplicationContext()).load(R.drawable.default_movie).into(posterIV);
                        }
                        else
                        {
                            Glide.with(getApplicationContext()).load(url).placeholder(R.drawable.default_movie).into(posterIV);
                        }



        }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.logIV:
            {
                Intent intent = new Intent(getApplicationContext(),HistoryActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.barIV:
            {
                Intent intent = new Intent(getApplicationContext(),BarCodeActivity.class);
                startActivity(intent);
                break;
            }
        }
    }
}
