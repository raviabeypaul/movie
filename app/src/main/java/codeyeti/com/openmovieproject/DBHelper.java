package codeyeti.com.openmovieproject;

/**
 * Created by ravi on 23/5/16.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

import codeyeti.com.openmovieproject.Model.HistoryModel;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    public static final String CONTACTS_TABLE_NAME = "movies";
    public static final String MOVIES_COLUMN_ID = "id";
    public static final String MOVIES_COLUMN_TITLE = "title";
    public static final String MOVIES_COLUMN_DATE = "date";
    public static final String MOVIES_COLUMN_TIME = "time";
//    public static final String CONTACTS_COLUMN_STREET = "street";
//    public static final String CONTACTS_COLUMN_CITY = "place";
//    public static final String CONTACTS_COLUMN_PHONE = "phone";
    private HashMap hp;

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table movies " +
                        "("+ MOVIES_COLUMN_ID +"integer primary key,"+MOVIES_COLUMN_TITLE + " text,"+ MOVIES_COLUMN_DATE+" text,"+MOVIES_COLUMN_TIME+" text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public boolean insertMovieDetails  (String title, String date, String time)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MOVIES_COLUMN_TITLE, title);
        contentValues.put(MOVIES_COLUMN_DATE, date);
        contentValues.put(MOVIES_COLUMN_TIME, time);
        db.insert("movies", null, contentValues);
        return true;
    }


    public ArrayList<HistoryModel> getAllMovies()
    {
        ArrayList<HistoryModel> array_list = new ArrayList<HistoryModel>();


        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from movies", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            HistoryModel ob = new HistoryModel();
            ob.setTitle(res.getString(res.getColumnIndex(MOVIES_COLUMN_TITLE)));
//            ob.setSno(res.getInt(res.getColumnIndex(MOVIES_COLUMN_ID)));
//            ob.setSno(1);
            ob.setDate(res.getString(res.getColumnIndex(MOVIES_COLUMN_DATE)));
            ob.setTime(res.getString(res.getColumnIndex(MOVIES_COLUMN_TIME)));
            array_list.add(ob);
            res.moveToNext();
        }
        return array_list;
    }
}