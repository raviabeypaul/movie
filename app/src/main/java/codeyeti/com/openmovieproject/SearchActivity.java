package codeyeti.com.openmovieproject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import codeyeti.com.openmovieproject.Controller.AppController;
import codeyeti.com.openmovieproject.Controller.Constants;
import codeyeti.com.openmovieproject.Model.MovieListModel;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {


    String title;
    Button submitBTN;
    EditText titleEt;
    LinearLayout linearLayout;
    DBHelper db;
    Toolbar toolbar;
    ImageView logIV, barIV;
    TextView titleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        init();

        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = titleEt.getText().toString();
                if (title.equals("")) {
                    Snackbar snackbar = Snackbar
                            .make(linearLayout, "Please Enter Name Of Movie", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    title.replace(" ", "+");
                    Log.i("title = ", title);
//                    Intent intent = new Intent(getApplicationContext(), MovieDetailActivity.class);
//                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE);
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putString("title", title);
                    getMovieList(title);
                    int month = Calendar.getInstance().get(Calendar.MONTH);
                    int date = Calendar.getInstance().get(Calendar.DATE);
                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    String current_date = String.valueOf(date) + "-" + String.valueOf(month) + "-" + String.valueOf(year);
                    Date d = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                    String currentDateTimeString = sdf.format(d);
                    db.insertMovieDetails(title, current_date, currentDateTimeString);
//                    editor.commit();
//                    startActivity(intent);
                }
            }
        });
    }

    private void init() {
        logIV = (ImageView) findViewById(R.id.logIV);
        barIV = (ImageView) findViewById(R.id.barIV);
        titleTV = (TextView) findViewById(R.id.toolbar_title);
        titleTV.setText("Home Page");
        logIV.setOnClickListener(this);
        barIV.setOnClickListener(this);
        toolbar  = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        db = new DBHelper(getApplicationContext());
        submitBTN  = (Button)  findViewById(R.id.submitBTN);
        titleEt = (EditText) findViewById(R.id.titleET);
        linearLayout = (LinearLayout) findViewById(R.id.mainContentLL);
    }

    private void getMovieList(final String title) {

        final ProgressDialog progressDialog = new ProgressDialog(SearchActivity.this, AlertDialog.THEME_HOLO_LIGHT);
        progressDialog.setMessage("please wait..");
        progressDialog.show();
        Log.i("title ", "getMovieList: " + title);
        String url = "http://www.omdbapi.com/?t=";
        url = url  + title + "&y=&plot=full&r=json";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.i("response of movies : ", response.toString());
                MovieListModel movieListModel = new MovieListModel();
                movieListModel = new Gson().fromJson(response.toString(),MovieListModel.class);
                Log.i("movieListModel", "onResponse: " + movieListModel.toString());
                if(movieListModel.getResponse().equals("True"))
                {
                    Intent intent = new Intent(getApplicationContext(),MovieDetailActivity.class);
                    intent.putExtra(Constants.actors,movieListModel.getActors());
                    intent.putExtra(Constants.director,movieListModel.getDirector());
                    intent.putExtra(Constants.title,movieListModel.getTitle());
                    intent.putExtra(Constants.year,movieListModel.getYear());
                    intent.putExtra(Constants.rated, movieListModel.getRated());
                    intent.putExtra(Constants.released, movieListModel.getReleased());
                    intent.putExtra(Constants.runtime, movieListModel.getRuntime());
                    intent.putExtra(Constants.genre, movieListModel.getGenre());
                    intent.putExtra(Constants.writer, movieListModel.getWriter());
                    intent.putExtra(Constants.plot, movieListModel.getPlot());
                    String url = movieListModel.getPoster();
                        intent.putExtra(Constants.imageUrl, url);
                    startActivity(intent);

                }
                else
                {
                    Snackbar snackbar = Snackbar
                            .make(linearLayout, "Movie Not Found " , Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Snackbar snackbar = null;
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    snackbar = Snackbar.make(linearLayout, "Retry", Snackbar.LENGTH_INDEFINITE);
                } else if (error instanceof AuthFailureError) {
                    snackbar = Snackbar.make(linearLayout, "Authentication Failure", Snackbar.LENGTH_INDEFINITE);
                } else if (error instanceof ServerError) {
                    snackbar = Snackbar.make(linearLayout, "Server Error", Snackbar.LENGTH_INDEFINITE);
                } else if (error instanceof NetworkError) {
                    snackbar = Snackbar.make(linearLayout, "Network Error", Snackbar.LENGTH_INDEFINITE);
                } else if (error instanceof ParseError) {
                    snackbar = Snackbar.make(linearLayout, "Network Error", Snackbar.LENGTH_INDEFINITE);
                }

                View view = snackbar.getView();
                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.WHITE);
                snackbar.setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getMovieList(title);
                    }
                }).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().add(jsonObjectRequest);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.logIV:
            {
                Intent intent = new Intent(getApplicationContext(),HistoryActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.barIV:
            {
                Intent intent = new Intent(getApplicationContext(),BarCodeActivity.class);
                startActivity(intent);
                break;
            }
        }
    }


//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest()

}
