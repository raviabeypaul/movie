package codeyeti.com.openmovieproject.Controller;

/**
 * Created by ravi on 20/5/16.
 */
public class Constants {
    public static String PREFERENCE_NAME = "pref_name";
    public static String   title = "title";
    public static String   year = "year";
    public static String   rated = "rated";
    public static String   released = "released";
    public static String   runtime = "runtime";
    public static String   genre = "genre";
    public static String   director = "director";
    public static String   writer = "writer";
    public static String   actors = "actors";
    public static String   plot = "plot";
    public static String   imageUrl = "imageUrl";

}
