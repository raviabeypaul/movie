package codeyeti.com.openmovieproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import codeyeti.com.openmovieproject.Adapter.MovieListAdapter;
import codeyeti.com.openmovieproject.Model.HistoryModel;

public class HistoryActivity extends AppCompatActivity implements View.OnClickListener {

    DBHelper myDb;
    RecyclerView listRV;
    MovieListAdapter movieListAdapter;
    ArrayList <HistoryModel> historyModels = new ArrayList<>();
    Toolbar toolbar;
    ImageView logIV,barIV;
    TextView titleTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        init();
    }

    private void init() {
        logIV = (ImageView) findViewById(R.id.logIV);
        barIV = (ImageView) findViewById(R.id.barIV);
        titleTV = (TextView) findViewById(R.id.toolbar_title);
        titleTV.setText("Movie Details");
        logIV.setOnClickListener(this);
        barIV.setOnClickListener(this);
        toolbar  = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myDb = new DBHelper(this);
        listRV = (RecyclerView) findViewById(R.id.listRV);
        historyModels = myDb.getAllMovies();
        movieListAdapter = new MovieListAdapter(historyModels);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listRV.setLayoutManager(mLayoutManager);
        listRV.setAdapter(movieListAdapter);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.logIV:
            {
                Intent intent = new Intent(getApplicationContext(),HistoryActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.barIV:
            {
                Intent intent = new Intent(getApplicationContext(),BarCodeActivity.class);
                startActivity(intent);
                break;
            }
        }
    }
}
